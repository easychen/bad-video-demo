import React from 'react';
import logo from './logo.svg';
import './App.css';

import PlayerContainer from 'griffith'

const duration = 182

const sources = {
  hd: {
    bitrate: 2005,
    size: 46723282,
    duration,
    format: 'mp4',
    width: 1280,
    height: 720,
    play_url: 'https://static.ftqq.com/resource-f392edb2f32fe3b2cb2fd58619ac798381b91602',
  },
  sd: {
    bitrate: 900.49,
    size: 20633151,
    duration,
    format: 'mp4',
    width: 320,
    height: 240,
    play_url: 'https://static.ftqq.com/resource-f392edb2f32fe3b2cb2fd58619ac798381b91602',
  },
}

const props = {
  id: 'zhihu2018',
  standalone: true,
  title: '2018 我们如何与世界相处？',
  cover: 'https://zhstatic.zhihu.com/cfe/griffith/zhihu2018.jpg',
  duration,
  sources,
  shouldObserveResize: true,
  src: 'mp4/zhihu2018_sd.mp4',
  useMSE: true,
}

function App() {
  return (
    <div className="App">
      <PlayerContainer {...props} />
    </div>
  );
}

export default App;
